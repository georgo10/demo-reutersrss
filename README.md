# reuters rss reader

Simple HTML RSS online reader without cache or storage
to fetch RSS feed (works only with RSS) and display
result on screen

## Usage (with maven):
* Download dependencies using: `mvn clean install`
* Run development server: `mvn appengine:devserver`