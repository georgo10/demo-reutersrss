<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Reuters RSS reader</title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext"/>
    <link type="text/css" rel="stylesheet" href="/stylesheets/reutersrss.css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>
<body>
<div id="loader" class="loading">&nbsp;</div>
<div id="content" class="hide"></div>
<script>
var url = "http://feeds.reuters.com/reuters/technologyNews";
$(document).ready(function() {
    $.getJSON("/get?url="+ encodeURIComponent(url), "", function(feed) {
        console.log(feed);
        $("#content").html("");
        // Feed title
        $("<h1/>", {
            'text': feed["title"]
        }).appendTo("#content");
        // Feed description
        $("<div/>", {
            'class': "description",
            'html': feed["description"]
        }).appendTo("#content");
        // Display items
        $.each(feed['items'], function() {
            // Item title
            $("<h2/>", {
                'html': $("<a/>", {
                    'text': this['title'],
                    'href': this['link']
                })
            }).appendTo("#content");
            // Item description
            $("<div/>", {
                'class': "description",
                'html': this["description"]
            }).appendTo("#content");
        })
        // Display container
        $("#loader").addClass("hide");
        $("#content").removeClass("hide");
	});
});
</script>
</body>
</html>