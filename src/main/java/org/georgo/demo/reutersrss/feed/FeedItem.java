package org.georgo.demo.reutersrss.feed;

/**
 * Feed item class
 * @author Tomas K. <iam@tomask.info>
 *
 */
public class FeedItem {
    String title;
    String link;
    String guid;
    String description;
    String pubDate;

    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getLink() {
        return link;
    }
    
    public void setLink(String link) {
        this.link = link;
    }
    
    public String getGuid() {
        return guid;
    }
    
    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getPubDate() {
        return pubDate;
    }
    
    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }
    
    @Override
    public String toString() {
        return title + "["+ link +"]: " + description;
    }

}
