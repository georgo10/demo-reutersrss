package org.georgo.demo.reutersrss.feed;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

/**
 * RSS Feed downloader and parser
 * @author Tomas K. <iam@tomask.info>
 *
 */
public class FeedParser {
    /* XML nodes */
    static final String TITLE = "title";
    static final String DESCRIPTION = "description";
    static final String CHANNEL = "channel";
    static final String LANGUAGE = "language";
    static final String COPYRIGHT = "copyright";
    static final String LINK = "link";
    static final String AUTHOR = "author";
    static final String ITEM = "item";
    static final String PUB_DATE = "pubDate";
    static final String GUID = "guid";
    /* RSS Feed URL */
    final String urlPath;
    final URL url;
    
    public FeedParser(String rssUrl) {
        try {
          this.url = new URL(rssUrl);
          urlPath = rssUrl;
        } catch (MalformedURLException e) {
          throw new RuntimeException(e);
        }
    }
    
    public Feed readFeed() {
        Feed feed = null;
        boolean isHeader = true;
        String title = "";
        String description = "";
        String copyright = "";
        String pubDate = "";
        String link = urlPath;
        String guid = "";
        
        try {
            // Read feed with XML reader
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = streamRead();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // Read nodes in XML (feed)
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                // Opening XML tag
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName().getLocalPart();
                    // Identify opening tag
                    switch (localPart) {
                    case ITEM:
                        // We are no longer in header
                        if (isHeader) {
                            isHeader = false;
                            feed = new Feed(title, description, copyright, pubDate, link);
                        }
                        break;
                    case TITLE:
                        title = getTextValue(event, eventReader);
                        break;
                    case DESCRIPTION:
                        description = getTextValue(event, eventReader);
                        break;
                    case COPYRIGHT:
                        copyright = getTextValue(event, eventReader);
                        break;
                    case PUB_DATE:
                        pubDate = getTextValue(event, eventReader);
                        break;
                    case LINK:
                        link = getTextValue(event, eventReader);
                        break;
                    case GUID:
                        guid = getTextValue(event, eventReader);
                        break;
                    }
                }
                else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
                        FeedItem feedItem = new FeedItem();
                        feedItem.setTitle(title);
                        feedItem.setDescription(description);
                        feedItem.setLink(link);
                        feedItem.setGuid(guid);
                        // Append item to feed
                        feed.getItems().add(feedItem);
                        // Cleanup
                        title = "";
                        description = "";
                        copyright = "";
                        pubDate = "";
                        link = urlPath;
                        guid = "";
                        
                        event = eventReader.nextEvent();
                        continue;
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        return feed;
    }
    
    /*
     * Input stream reader
     */
    private InputStream streamRead() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /*
     * Get text value from XML node
     */
    private String getTextValue(XMLEvent event, XMLEventReader eventReader)
            throws XMLStreamException {
        String textValue = "";
        Boolean reading = false;
        do {
            reading = false;
            event = eventReader.nextEvent();
            if (event instanceof Characters) {
                textValue = textValue + event.asCharacters().getData();
                reading = true;
            }
        } while(reading);
        
        return textValue;
    }
}
